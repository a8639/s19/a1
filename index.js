let num = 2;
var getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}`);

var address = {
    street: "258 Washington Ave NW",
    state: "California",
    zip: 90011
}
console.log(`I live at ${address.street}, ${address.state} ${address.zip}`);




let animal = {
    name: "Lolong",
    type: "saltwater crocodile",
    weight: 1075,
    measurementFt: 20,
    measurementIn: 3
}

function describeAnimal (crocodile){
   
    let {name, type, weight, measurementFt, measurementIn} = crocodile;
    
    return `${name} was a ${type}. He weighed ${weight} kgs with a measurement of ${measurementFt}ft ${measurementIn}in.`
}
console.log(describeAnimal(animal));



let numbers = [1, 2, 3, 4, 5];

/*
let numberList = numbers.forEach(
    function(element){
        console.log(element)
    }
);
*/
numbers.forEach(data => console.log(data));

class Dog{
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed
    }
}
let dog1 = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dog1);